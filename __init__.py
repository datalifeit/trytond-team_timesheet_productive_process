# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import team_timesheet


def register():
    Pool.register(
        team_timesheet.TeamTimesheet,
        team_timesheet.TeamTimesheetWork,
        module='team_timesheet_productive_process', type_='model')
